package net.ddns.ppls.gge_vertretung;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlertDialog a = new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_text)
                .setOnDismissListener(dialog -> {
                    MainActivity.this.finish();
                })
                .setPositiveButton(R.string.dialog_action, (dialog, which) -> {
                    Intent i = new Intent(Intent.ACTION_DELETE);
                    i.setData(Uri.parse("package:"+getApplicationContext().getPackageName()));
                    startActivity(i);
                })
                .setNeutralButton("ppluss.de", (dialog, which) -> {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://ppluss.de/?from=gge-uninstaller"));
                    startActivity(i);
                })
                .show();
    }
}